import React, { Component } from 'react'
// import jwt from 'jsonwebtoken';
// import axios from 'axios'
import LoginPageUi from './LoginPageUi'
import NotesPage from '../Notes/NotesPage'
import axios from 'axios'

class LoginPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userid: "",
            password: "",
            isLogged: false,
            loginStatus: false
        }

        this.userDetails = {
            username: "test",
            password: "test"
        }
    }

    componentDidMount() {
        this.storeCollector()
    }

    storeCollector() {
        let store = JSON.parse(localStorage.getItem('login'));
        if (store && store.login) {
            this.setState({
                isLogged: true,
                loginStatus: true
            })
        }
    }

    loginHandler = async (event) => {
        console.log("inside login handler", this.state);
        let loginRes = await axios({
            method: 'POST',
            url: 'http://localhost:4000/',
            data: {
                userId: this.state.userid,
                password: this.state.password
            }
        })
        console.log("login response", loginRes)
        if (loginRes.data.success) {
            localStorage.setItem('login', JSON.stringify(loginRes.data))
            this.storeCollector()
        } else {
            this.setState({
                loginStatus: loginRes.data.loginStatus
            })
        }
    }

    usernameChangeHandler = (event) => {
        console.log("user id", event.target.value);
        this.setState({
            userid: event.target.value
        })
    }

    passwordChangeHandler = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    render() {
        let notesPage,errormsg;
        let display = <LoginPageUi usernameChangeHandler={this.usernameChangeHandler} passwordChangeHandler={this.passwordChangeHandler} loginHandler={this.loginHandler} />
        if (this.state.isLogged) {
            display = <h1>Notes</h1>
            notesPage = <NotesPage />
        }
        if(this.state.loginStatus == "wrong credentials"){
            errormsg = <span className="text-danger" > Enter valid credentials </span>
        }

        return (
            <div>
                <div className="container jumbotron d-flex flex-column justify-content-center" id="bg-active">
                    {display}
                    {errormsg}
                </div>
                {notesPage}
            </div>
        )
    }
}

export default LoginPage
