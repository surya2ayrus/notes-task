import React from "react";
import { hot } from 'react-hot-loader/root';
import LoginPage from './Login/LoginPage'
import '../css/login.css'
import MapNotes from './Map/MapNotes'

class App extends React.Component {    
   render() {   
      return (
            <div>
                <LoginPage />
                  {/* <MapNotes /> */}
            </div>
      );
   }
}
export default hot(App);