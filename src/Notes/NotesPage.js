import React, { Component } from 'react'

let db = null;
let objectStore = null;
let version = 3;
let dbOpenReq;
export class NotesPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title: "",
            content: "",
            file: "",
            imageNotesArr: [],
            textNotesArr: []
        }
        this.intialState = this.state
        this.initializeDB();

    }

    generateId(){
        let id = (Math.floor((Math.random()/Math.random())*1000)) + "-" + (Math.floor((Math.random()/Math.random())*1000));
        console.log("id generate",id)
        return id
    }

    initializeDB(title, content) {
        // version = version+1;    
        dbOpenReq = indexedDB.open("NotesDB", version);
        dbOpenReq.onerror = function (err) {
            console.log("error in creating db", err)
        }

        dbOpenReq.onsuccess = function (event) {
            db = event.target.result;
            console.log("db value after success", db)
        }

        dbOpenReq.onupgradeneeded = (event) => {
            db = event.target.result;
            console.log("db value in upgrade needed", db)
            if (!db.objectStoreNames.contains('textNotes')) {
                objectStore = db.createObjectStore('textNotes', {
                    keyPath: "id"
                })
            }
            if (!db.objectStoreNames.contains('imageNotes')) {
                objectStore = db.createObjectStore('imageNotes', {
                    keyPath: "id"
                })
            }

        }

    }

    getPostInfo = (event) => {
        let title, content, file;
        let data = {};
        event.preventDefault();
        console.log("data in form", event);
        if (this.state.title && this.state.content && !this.state.file) {
            title = this.state.title;
            content = this.state.content;
            data = {
                id: this.generateId(),
                title,
                content
            }
            this.createNotes('textNotes', 'readwrite', data)
        }
        if (this.state.title && this.state.content && this.state.file) {
            title = this.state.title;
            content = this.state.content;
            file = this.state.file;
            data = {
                id: this.generateId(),
                title,
                content,
                file
            }
            this.createNotes('imageNotes', 'readwrite', data)
        }
        this.reset();
    }

    makeTx = async (storeName, mode) => {
        let tx = db.transaction(storeName, mode);
        tx.onerror = (err) => {
            console.log("Error in transaction", err);
        }
        return tx;
    }

    createNotes = async (storeName, mode, storeData) => {

        console.log("notes json", storeData)
        let tx = await this.makeTx(storeName, mode);

        tx.oncomplete = (event) => {
            console.log("completed", event);
        }

        let store = tx.objectStore(storeName);
        let request = store.add(storeData)

        request.onsuccess = (event) => {
            console.log("successfully added an object", event);
        }

        request.onerror = (err) => {
            console.log("error in req obj", err);
        }
    }

    setTitle = (event) => {
        this.setState({
            title: event.target.value
        })
    }

    setFile = (event) => {
        let reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (e) => {
            this.setState({
                file: reader.result
            })
        }
    }

    setContent = (event) => {
        this.setState({
            content: event.target.value
        })
    }

    showNotes = (event) = async () => {
        // let tx = await this.makeTx('textNotes', 'readonly');
        // tx.oncomplete = (event) => {
        //     console.log("completed reading");
        // }
        // let store = tx.objectStore('textNotes');
        // let textNotesArr = store.getAll();
        // console.log("notes arr", textNotesArr);

        // tx = await this.makeTx('imageNotes','readonly');
        // tx.oncomplete = (event) => {
        //     console.log("completed reading image notes");
        // }
        // store = tx.objectStore('imageNotes');
        // let imageNotesArr = store.getAll();
        await this.fetchNotes('textNotes', 'readonly');
        await this.fetchNotes('imageNotes', 'readonly');
    }

    fetchNotes = async (storeName, mode) => {
        let req, notesArr, store
        let tx = await this.makeTx(storeName, mode);
        tx.oncomplete = (event) => {
            console.log("completed reading ", storeName, " notes");
        }
        store = await tx.objectStore(storeName);
        req = await store.getAll();

        req.onerror = (err) => {
            console.warn("error in fetching data", err);
        }

        req.onsuccess = (event) => {
            let req = event.target
            notesArr = req.result;
            this.storeDataIntoState(notesArr, storeName + "Arr");
        }

    }


    storeDataIntoState(dataArr, stateArrName) {
        console.log("inside store data into state", dataArr, stateArrName);
        this.setState({
            [stateArrName]: [...dataArr]
        }, () => {
            console.log("state output", this.state);
        })
    }

    deletePost = async(event) => {
        console.log("event value in delete",event.target.dataset.key,event);
        let storeName;
        let key = event.target.dataset.key;
        if(event.target.parentElement.children.length == 3){
            storeName = "textNotes"
        }
        if(event.target.parentElement.children.length == 4){
            storeName = "imageNotes"
        }
        let tx = await this.makeTx(storeName,'readwrite')

        tx.oncomplete = (event) => {
            console.log("completed deleting");
            this.showNotes();
        }
        let store = tx.objectStore(storeName);
        let req = store.delete(key);

        req.onsuccess = (event) => {
            console.log("successfully deleted")
        }
        req.onerror = (event) => {
            console.log("error in deleting")
        }
    }

    reset(){
        console.log("inside reset")
        this.setState(this.intialState)
    }

    render() {

        let displayImageNotes, displayTextNotes;
        
        if (this.state.imageNotesArr) {
            displayImageNotes = this.state.imageNotesArr.map(post => {
                return <div className="postsContainer">
                    <div className="post" key={post.id}>
                        <div style={{ backgroundImage: "url(" + post.file + ")" }} />
                        <h2>{post.title}</h2>
                        <p>{post.content}</p>
                        <button className="delete" data-key={post.id} onClick={this.deletePost}>Delete</button>
                    </div>
                </div>
            })
        }

        if(this.state.textNotesArr){
            displayTextNotes = this.state.textNotesArr.map(post => {
                return <div className="postsContainer">
                    <div className="post" key={post.id}>
                        <h2>{post.title}</h2>
                        <p>{post.content}</p>
                        <button className="delete" data-key={post.id} onClick={this.deletePost}>Delete</button>
                    </div>
                </div>
            })
        }

        return (
            <div>
                <div className="center-div">
                    <form onSubmit={this.getPostInfo}>
                        <div className="control">
                            <label>Title</label>
                            <input type="text" name="title" className="form-decoration" value={this.state.title} onChange={this.setTitle} />
                        </div>
                        <div className="control">
                            <label>Content</label>
                            <textarea name="content" className="form-decoration" value={this.state.content} onChange={this.setContent} />
                        </div>
                        <div className="control">
                            <input type="file" id="cover" className="form-decoration" name="file" value={this.state.file} onChange={this.setFile} />
                        </div>
                        <input type="submit" className="btn-decoration" value="Submit" />
                    </form>
                    <input type="submit" className="btn-decoration" value="Show Notes" onClick={this.showNotes} />
                </div>
                <div className="d-flex flex-row justify-content-between">
                    <div className="col-4">
                        {displayTextNotes}
                    </div>
                    <div className="col-4">
                        {displayImageNotes}
                    </div>
                    <div className="col-4">

                    </div>
                </div>
            </div>
        )
    }
}

export default NotesPage
