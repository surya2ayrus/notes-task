const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    next();
  });

app.listen(4000,function(){
    console.log("listening to port 4000")
})

app.use(bodyParser.json());

app.post("/",(req,res) => {
    console.log("req body",req.body)
    let userCredentials = {
        userId: "test",
        password: "test"
    }
    let userId = req.body.userId;
    let password = req.body.password;
    if(userCredentials.userId == userId && userCredentials.password == password){
        let token = jwt.sign(userCredentials,"secret",{
            expiresIn: '1h'
        })
        console.log("success")
        res.send({
            success: true,
            token: "JWT "+token,
            login: true
        })
    }
    res.send({
        success: false,
        login: false,
        loginStatus: 'wrong credentials'
    })

})