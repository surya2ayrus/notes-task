To Initialize the project follow the steps down below:

1. To install the dependencies open the terminal and change directory to notes-task and type `npm install`
2. Dependencies for front-end will be downloaded.
3. To install the dependencies of back-end open terminal and change directory to notes-task/server and type `npm install`
4. Server Dependencies will be downloaded

To Run the Project

1. In the other terminal run `node index.js` to start the server.
2. Open the terminal and change directory to notes-task and run `npm start` and go to `http://localhost:8080` to see the changes
3. To Login Credentials are `userid - test` and `password - test`



There you go! you project is initialized 